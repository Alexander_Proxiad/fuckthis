$(document).ready(function () {
    var isViewSwapped = false;
    var numberOfTimesButtonIsClicked = 0;
    $("#forceSubmit").click(function (e) {
        e.preventDefault();
        var hero = $("#selectCharacter option:selected").val();
        switch (hero) {
            case "1":
                $("#profilePicture").attr("src", "profile-pictures/profile-luke.jpg");
                break;
            case "2":
                $("#profilePicture").attr("src", "profile-pictures/profile-c3po.jpg");
                break;
            case "3":
                $("#profilePicture").attr("src", "profile-pictures/profile-r2d2.jpg");
                break;
            case "4":
                $("#profilePicture").attr("src", "profile-pictures/profile-darth-vader.png");
                break;
            case "5":
                $("#profilePicture").attr("src", "profile-pictures/profile-leia.png");
                break;
            case "6":
                $("#profilePicture").attr("src", "profile-pictures/profile-owen.jpg");
                break;
            case "7":
                $("#profilePicture").attr("src", "profile-pictures/profile-beru.png");
                break;
            case "8":
                $("#profilePicture").attr("src", "profile-pictures/profile-r5d4.png");
                break;
            case "9":
                $("#profilePicture").attr("src", "profile-pictures/profile-biggs.png");
                break;
            case "10":
                $("#profilePicture").attr("src", "profile-pictures/profile-obi-wan.png");
                break;
            default:
                break;
        }
        var textAreaTest = $("#commentCharacter").val();
        $("#profilePicture").attr("title", textAreaTest);

        var starWarsCharacter = $("#selectCharacter").val();
        $.ajax({
            method: "GET",
            url: "https://swapi.co/api/people/" + starWarsCharacter
        }).then((data) => {
            console.log(data);
            if ($("#checkBirth").prop("checked") === false) {
                data.birth_year = "";
            }
            if ($("#checkHeight").prop("checked") === false) {
                data.height = "";
            }
            if ($("#checkGender").prop("checked") === false) {
                data.gender = "";
            }
            $("#displayResultRows").append(
                "<tr>"
                + "<td>" + data.name + "</td>" +
                "<td>" + data.birth_year + "</td>" +
                "<td>" + data.height + "</td>" +
                "<td>" + data.gender + "</td>" +
                "</tr>"
            );
            if (isViewSwapped === true) {
                switchViewFancy()
            }
        });
    });

    $("#phpButton").click(function () {
        var imageUrl = "background-pics/phphaha.png";
        $("body").css('background-image', 'url(' + imageUrl + ')');
    });
    $("#switchViewFancy").click(function () {
        isViewSwapped = true;
        switchViewFancy()
    })
    $("#switchViewNormal").click(function () {
        isViewSwapped = false;
        switchViewNormal()
    })
    function switchViewFancy() {
        $("table").css({
            'border': '1px solid #ddd',
            'border-collapse': 'separate',
            'border-left': '0',
            'border-radius': '20px',
            'border-spacing': '0px',
            '-webkit-box-shadow': '0px 0px 50px 30px #ff0000',
            '-moz-box-shadow': '0px 0px 50px 30px #ff0000',
            'box-shadow': '0px 0px 50px 30px #ff0000',
            'text-shadow': '1px 1px 2px black, 0 0 25px blue, 0 0 5px darkblue',
        })
        $("tr").css({
            'display': 'table-row',
            'vertical-align': 'inherit',
            'border-color': 'inherit',
            'border-radius': '50px',
            'background-image': 'linear-gradient(to right top, #98f9ba, #a1bb6c, #967e3c, #7a4625, #4f1419)',
        })
        $("th").css({
            'padding': '5px 4px 6px 4px',
            'vertical-align': 'top',
            'border-left': '1px solid #ddd',
            'border-radius': '50px',
            'height': '100px',
            'line-height': '100px',
            'text-align': 'center',
            'font-size': '150%',
        })
        $("td").css({
            'padding': '5px 4px 6px 4px',
            'vertical-align': 'top',
            'border-left': '1px solid #ddd',
            'border-top': '1px solid #ddd',
            'border-radius': '50px',
            'height': '100px',
            'line-height': '100px',
            'text-align': 'center',
            'font-size': '150%',
        })
    }
    function switchViewNormal() {
        $("table").css({
            'border': '',
            'border-collapse': '',
            'border-left': '',
            'border-radius': '',
            'border-spacing': '',
            '-webkit-box-shadow': '',
            '-moz-box-shadow': '',
            'box-shadow': '',
            'text-shadow': '',
        })
        $("tr").css({
            'display': '',
            'vertical-align': '',
            'border-color': '',
            'border-radius': '',
            'background-image': '',
        })
        $("th").css({
            'padding': '',
            'text-align': '',
            'vertical-align': '',
            'border-left': '',
            'border-radius': '',
            'height': '',
            'text-align': '',
            'font-size': '',
            'line-height': '',
        })
        $("td").css({
            'padding': '',
            'text-align': '',
            'vertical-align': '',
            'border-left': '',
            'border-top': '',
            'border-radius': '',
            'height': '',
            'text-align': '',
            'font-size': '',
            'line-height': '',
        })
    }

});