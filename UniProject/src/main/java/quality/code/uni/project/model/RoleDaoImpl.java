package quality.code.uni.project.model;

import java.util.ArrayList;
import java.util.List;

import org.hibernate.criterion.Restrictions;
import org.springframework.stereotype.Repository;

import quality.code.uni.project.utils.RoleEnum;

@Repository
public class RoleDaoImpl extends GenericDaoImpl<Role> implements RoleDao {

	@SuppressWarnings({ "unchecked", "deprecation" })
	@Override
	public List<Role> getAllRoles() {
		return getSession().createCriteria(Role.class).list();
	}

	@SuppressWarnings({ "unchecked", "deprecation" })
	@Override
	public List<Role> getDeveloperRoles() {
		return getSession().createCriteria(Role.class).add(Restrictions.eq("role", RoleEnum.DEVELOPER.getName()))
				.list();
	}

	@SuppressWarnings({ "deprecation", "unchecked" })
	@Override
	public List<Role> getManagerRoles() {
		List<String> roleList = new ArrayList<>();
		roleList.add(RoleEnum.DEVELOPER.getName());
		roleList.add(RoleEnum.MANAGER.getName());
		return getSession().createCriteria(Role.class).add(Restrictions.in("role", roleList)).list();
	}

	@SuppressWarnings({ "deprecation", "unchecked" })
	@Override
	public List<Role> getHumanResourcesRoles() {
		return getSession().createCriteria(Role.class).add(Restrictions.eq("role", RoleEnum.HUMAN_RESOURCES.getName()))
				.list();
	}

}
