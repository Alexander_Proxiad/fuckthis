package quality.code.uni.project.model;

import java.io.IOException;
import java.sql.SQLException;
import java.util.List;

import javax.servlet.http.HttpServletResponse;

import org.springframework.web.multipart.MultipartFile;

public interface UserService {

	/**
	 * Method getting user from database based on a specific id
	 * 
	 * @param loginUser
	 * @return
	 */
	User extractUserFromDatabase(LoginBean loginUser);

	/**
	 * Method that returns User entity from the database based on id
	 * 
	 * @param id
	 * @return
	 */
	User getUser(int id);

	/**
	 * Method that creates a criteria returning the 3 newest registered users from
	 * the database
	 * 
	 * @return
	 */
	List<User> getThreeNewestUsers();

	/**
	 * Method extracting all users present in the database
	 * 
	 * @return List<User>
	 */
	List<User> getAllUsers();

	/**
	 * Method saving a user with blob to the database
	 * 
	 * @param user  User
	 * @param photo MultipartFile
	 */
	void saveUserWithBlob(User user, MultipartFile photo);

	/**
	 * Method extracting the blob for every user from the database and later is
	 * being called in the user interface
	 * 
	 * @param response HttpServletResponse
	 * @param id       int
	 * @throws SQLException
	 * @throws IOException
	 */
	void extractEmployeePicture(HttpServletResponse response, int id) throws SQLException, IOException;

}
