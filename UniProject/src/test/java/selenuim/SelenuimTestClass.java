package selenuim;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

import java.util.logging.Logger;

import org.apache.commons.lang.RandomStringUtils;
import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;

/**
 * Class performing automation tests using selenium over the
 * http://automationpractice.com/index.php website
 * 
 * @author a.vulchev
 *
 */
public class SelenuimTestClass {

	private Logger myLogger = Logger.getLogger(getClass().getName());
	static WebDriver driver;

	/**
	 * Setting commons - chrome driver,coockies,site
	 */
	@BeforeClass
	public static void invokeBrowser() {
		System.setProperty("webdriver.chrome.driver", "C:\\Users\\Hack\\Desktop\\HAX\\driver\\chromedriver.exe");
		driver = new ChromeDriver();
		driver.manage().deleteAllCookies();
		driver.manage().window().maximize();
		driver.get("localhost:8080/login");
	}

	/**
	 * Methods testing the title of the website
	 * 
	 * @throws InterruptedException
	 */
	@Test
	public void testTitle() throws InterruptedException {
		String actualTitle = driver.getTitle();
		String expectedTitle = "ALEXNET";
		Thread.sleep(5000);
		assertEquals(actualTitle, expectedTitle);
		myLogger.info("<<<<< Title test SUCCESS >>>>>");
	}

	@Test
	public void testLoginForm() throws InterruptedException {
		WebElement emailInputField = driver.findElement(By.xpath("//*[@id=\"login-firstName\"]"));
		emailInputField.sendKeys("aspeks@mail.bg");
		WebElement password = driver.findElement(By.xpath("//*[@id=\"login-password\"]"));
		password.sendKeys("123456");
		WebElement loginButton = driver.findElement(By.xpath("//*[@id=\"loginform\"]/div[3]/div/input"));
		loginButton.click();
		assertTrue(driver.getPageSource().contains("Alexander"));
	}

	@Test
	public void testLoginAndLogOut() throws InterruptedException {
		WebElement emailInputField = driver.findElement(By.xpath("//*[@id=\"login-firstName\"]"));
		emailInputField.sendKeys("aspeks@mail.bg");
		WebElement password = driver.findElement(By.xpath("//*[@id=\"login-password\"]"));
		password.sendKeys("123456");
		WebElement loginButton = driver.findElement(By.xpath("//*[@id=\"loginform\"]/div[3]/div/input"));
		loginButton.click();
		assertTrue(driver.getPageSource().contains("Alexander"));
		WebElement logoutButton = driver.findElement(By.xpath("//*[@id=\"modal-383649\"]"));
		logoutButton.click();
		WebElement confirmLogout = driver.findElement(By.xpath("//*[@id=\"command\"]/input"));
		confirmLogout.click();
		Thread.sleep(3000);
		assertFalse(driver.getPageSource().contains("Alexander"));
	}

	@Test
	public void testLoginAndPublishIdea() throws InterruptedException {
		WebElement emailInputField = driver.findElement(By.xpath("//*[@id=\"login-firstName\"]"));
		emailInputField.sendKeys("aspeks@mail.bg");
		WebElement password = driver.findElement(By.xpath("//*[@id=\"login-password\"]"));
		password.sendKeys("123456");
		WebElement loginButton = driver.findElement(By.xpath("//*[@id=\"loginform\"]/div[3]/div/input"));
		loginButton.click();
		WebElement createNewIdeaLink = driver.findElement(By.xpath("//*[@id=\"modal-761057\"]"));
		createNewIdeaLink.click();
		WebElement ideaTitleInputField = driver.findElement(By.xpath("//*[@id=\"topicTitle\"]"));
		String randomIdeaTitle = RandomStringUtils.randomAlphabetic(10);
		ideaTitleInputField.sendKeys(randomIdeaTitle);
		WebElement ideaDescriptionInputField = driver.findElement(By.xpath("//*[@id=\"comment\"]"));
		String randomIdeaDescription = RandomStringUtils.randomAlphabetic(50);
		ideaDescriptionInputField.sendKeys(randomIdeaDescription);
		WebElement ideaCreationFromSubmitButton = driver.findElement(By.xpath("//*[@id=\"saveTopic\"]/input[1]"));
		Thread.sleep(3000);
		ideaCreationFromSubmitButton.click();
		assertTrue(driver.getPageSource().contains(randomIdeaTitle));
		assertTrue(driver.getPageSource().contains(randomIdeaDescription));
	}

	@Test
	public void testFilterSection() throws InterruptedException {
		WebElement emailInputField = driver.findElement(By.xpath("//*[@id=\"login-firstName\"]"));
		emailInputField.sendKeys("aspeks@mail.bg");
		WebElement password = driver.findElement(By.xpath("//*[@id=\"login-password\"]"));
		password.sendKeys("123456");
		WebElement loginButton = driver.findElement(By.xpath("//*[@id=\"loginform\"]/div[3]/div/input"));
		loginButton.click();
		WebElement filterSectionLink = driver
				.findElement(By.xpath("//*[@id=\"bs-example-navbar-collapse-1\"]/ul[1]/li[3]/a"));
		filterSectionLink.click();
		WebElement filterInputField = driver.findElement(By.xpath("//*[@id=\"search\"]"));
		filterInputField.sendKeys("Margaritov");
		Thread.sleep(2000);
		assertTrue(driver.getPageSource().contains("Margaritov"));
	}

	/**
	 * Method closing the Chrome driver
	 */
	@AfterClass
	public static void closeBrowser() {
		driver.quit();
	}

}
