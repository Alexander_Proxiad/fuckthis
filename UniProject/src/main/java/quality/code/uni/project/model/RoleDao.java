package quality.code.uni.project.model;

import java.util.List;

public interface RoleDao extends GenericDao<Role> {

	/**
	 * Method extracting all roles from the database
	 * 
	 * @return List<Role>
	 */
	List<Role> getAllRoles();

	/**
	 * Method extracting all the developer roles from the database
	 * 
	 * @return List<Role>
	 */
	List<Role> getDeveloperRoles();

	/**
	 * Method extracting all the manager roles from the database
	 * 
	 * @return List<Role>
	 */
	List<Role> getManagerRoles();

	/**
	 * Method extracting all the human resources roles from the database
	 * 
	 * @return List<Role>
	 */
	List<Role> getHumanResourcesRoles();

}
