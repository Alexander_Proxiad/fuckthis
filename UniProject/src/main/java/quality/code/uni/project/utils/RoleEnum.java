package quality.code.uni.project.utils;

public enum RoleEnum {

	DEVELOPER("DEVELOPER"),
	MANAGER("MANAGER"),
	HUMAN_RESOURCES("HUMAN RESOURCES"),
	ADMIN("ADMIN");

	private String name;

	public String getName() {
		return this.name;
	}

	private RoleEnum(String action) {
		this.name = action;
	}
}