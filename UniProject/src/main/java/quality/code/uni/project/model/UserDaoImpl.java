package quality.code.uni.project.model;

import java.io.IOException;
import java.sql.Blob;
import java.util.List;

import org.hibernate.Hibernate;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Restrictions;
import org.springframework.stereotype.Repository;
import org.springframework.web.multipart.MultipartFile;

@Repository
public class UserDaoImpl extends GenericDaoImpl<User> implements UserDao {

	@SuppressWarnings("deprecation")
	@Override
	public User extractUserFromDatabase(LoginBean loginUser) {
		return (User) getSession().createCriteria(User.class, "user")
				.add(Restrictions.eq("user.email", loginUser.getEmail())).uniqueResult();

	}

	@SuppressWarnings({ "deprecation", "unchecked" })
	@Override
	public List<User> getThreeNewestUsers() {
		return getSession().createCriteria(User.class).setMaxResults(3).addOrder(Order.desc("id")).list();
	}

	@Override
	public void saveUserWithBlob(User user, MultipartFile photo) {
		try {
			Blob blob = Hibernate.getLobCreator(getSession()).createBlob(photo.getBytes());
			user.setPhoto(blob);
			save(user);
		} catch (IOException e) {
			e.printStackTrace();
		}

	}

}
