package quality.code.uni.project.model;

import java.util.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class TopicServiceImpl implements TopicService {

	@Autowired
	private UserService userService;

	@Autowired
	private CommentService commentService;

	@Autowired
	private TopicDao topicDao;

	@Override
	public List<Idea> getTopics() {
		return topicDao.getTopics();
	}

	@Override
	public List<Idea> saveCommentToTopicAndSetAllTopics(NewCommentBean commentBean) {
		Idea currentIdea = topicDao.get(commentBean.getTopicId());
		User currentUser = userService.getUser(commentBean.getUserId());
		Comment comment = new Comment(commentBean.getComment(), new Date(), currentIdea, currentUser);
		commentService.saveComment(comment);
		List<Idea> ideaTopics = topicDao.getIdeasAscending();
		Idea thisIdea = ideaTopics.get(commentBean.getTopicId() - 1);
		thisIdea.getComments().add(comment);
		return ideaTopics;
	}

	@Override
	public List<Idea> saveIdeaAndSetAllIdeas(IdeaBean ideaBean) {
		User currentUser = userService.getUser(ideaBean.getId());
		Idea idea = new Idea(ideaBean.getTitle(), ideaBean.getDescription(), new Date(), null, currentUser);
		topicDao.save(idea);
		List<Idea> ideaTopics = topicDao.getTopics();
		return ideaTopics;
	}

}
