package quality.code.uni.project.model;

import java.sql.Blob;
import java.util.Date;
import java.util.List;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.Lob;
import javax.persistence.ManyToMany;
import javax.persistence.OneToMany;
import javax.persistence.Table;

/**
 * 
 * User entity consisting of the following set of parameters: The id of the
 * user,first name,last name,age,gender and role which are stored in the
 * FORUM_USER table in the database;
 * 
 * @author Hack
 *
 */
@Entity
@Table(name = "FORUM_USER")
public class User {

	// add place in the company ex. Juniour developer
	// add short description

	@Id
	@Column(name = "user_id", nullable = false, unique = true)
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private int id;

	@Column(name = "first_name", nullable = false, length = 100)
	private String firstName;

	@Column(name = "password", nullable = false, length = 100)
	private String password;

	@Column(name = "email", nullable = false, length = 100)
	private String email;

	@Column(name = "last_name", nullable = true, length = 100)
	private String lastName;

	@Column(name = "age", nullable = true, length = 3)
	private int age;

	@Column(name = "gender", nullable = false)
	private boolean gender;

	@ManyToMany(cascade = CascadeType.ALL)
	@JoinTable(name = "auth_user_role", joinColumns = @JoinColumn(name = "auth_user_id"), inverseJoinColumns = @JoinColumn(name = "auth_role_id"))
	private List<Role> roles;

	@Column(name = "joined_on", nullable = false)
	private Date joinedOn;

	@Column(name = "active", columnDefinition = "int default 1")
	private int active;

	@Lob
	@Column(name = "photo", columnDefinition = "LONGBLOB", nullable = false)
	private Blob photo;

	@OneToMany(mappedBy = "user")
	private List<Comment> comments;

	@OneToMany(mappedBy = "user")
	private List<Idea> topics;

	private String authority;

	/**
	 * No-args constructor for the User Entity
	 */
	public User() {
		super();
	}

	public User(String firstName, String password, String email, String lastName, int age, boolean gender,
			List<Role> roles, Date joinedOn, int active, Blob photo, List<Comment> comments, List<Idea> topics,
			String authority) {
		super();
		this.firstName = firstName;
		this.password = password;
		this.email = email;
		this.lastName = lastName;
		this.age = age;
		this.gender = gender;
		this.roles = roles;
		this.joinedOn = joinedOn;
		this.active = active;
		this.photo = photo;
		this.comments = comments;
		this.topics = topics;
		this.authority = authority;
	}

	public int getAge() {
		return age;
	}

	public List<Comment> getComments() {
		return comments;
	}

	public String getEmail() {
		return email;
	}

	public String getFirstName() {
		return firstName;
	}

	public int getId() {
		return id;
	}

	public Date getJoinedOn() {
		return joinedOn;
	}

	public String getLastName() {
		return lastName;
	}

	public String getPassword() {
		return password;
	}

	public Blob getPhoto() {
		return photo;
	}

	public List<Role> getRoles() {
		return roles;
	}

	public List<Idea> getTopics() {
		return topics;
	}

	public boolean isGender() {
		return gender;
	}

	public void setAge(int age) {
		this.age = age;
	}

	public void setComments(List<Comment> comments) {
		this.comments = comments;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	public void setGender(boolean gender) {
		this.gender = gender;
	}

	public void setId(int id) {
		this.id = id;
	}

	public void setJoinedOn(Date joinedOn) {
		this.joinedOn = joinedOn;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public void setPhoto(Blob photo) {
		this.photo = photo;
	}

	public void setRoles(List<Role> roles) {
		this.roles = roles;
	}

	public void setTopics(List<Idea> topics) {
		this.topics = topics;
	}

	public int getActive() {
		return active;
	}

	public void setActive(int active) {
		this.active = active;
	}

	public String getAuthority() {
		return authority;
	}

	public void setAuthority(String authority) {
		this.authority = authority;
	}

	@Override
	public String toString() {
		return "User [id=" + id + ", firstName=" + firstName + ", password=" + password + ", email=" + email
				+ ", lastName=" + lastName + ", age=" + age + ", gender=" + gender + ", roles=" + roles + ", joinedOn="
				+ joinedOn + ", photo=" + photo + ", comments=" + comments + ", topics=" + topics + "]";
	}

}
