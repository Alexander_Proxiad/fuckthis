package quality.code.uni.project.model;

import java.util.List;

import org.hibernate.criterion.Restrictions;
import org.springframework.stereotype.Repository;

@Repository
public class CommentDaoImpl extends GenericDaoImpl<Comment> implements CommentDao {

	@SuppressWarnings({ "unchecked", "deprecation" })
	@Override
	public List<Comment> getCommentsForIdeaWithIdeaId(int id) {
		return getSession().createCriteria(Comment.class).add(Restrictions.eq("idea.id", id)).list();
	}

}
