package quality.code.uni.project.model;

/**
 * Login bean has the sole purpose to store the parameters from the login form
 * of the forum
 * 
 * @author a.vulchev
 *
 */
public class LoginBean {

	private String email;
	private String password;

	/**
	 * No-args constructor
	 */
	public LoginBean() {
		super();
	}

	/**
	 * Full parameter constructor
	 * 
	 * @param firstName
	 * @param password
	 */
	public LoginBean(String email, String password) {
		super();
		this.email = email;
		this.password = password;
	}

	public String getEmail() {
		return email;
	}

	public String getPassword() {
		return password;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return "LoginBean [firstName=" + email + ", password=" + password + "]";
	}

}
