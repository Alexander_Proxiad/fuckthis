package quality.code.uni.project.model;

import java.io.Serializable;
import java.util.List;

/**
 * Generic interface that performs all the basic CRUD operations and accepts all
 * types of objects
 * 
 * @author a.vulchev
 *
 * @param <T>
 */
public interface GenericDao<T> {

	/**
	 * Saves the given object to the database
	 * 
	 * @param o
	 * @return
	 */
	Serializable save(final T o);

	/**
	 * Deletes an object from the database based on provided id
	 * 
	 * @param id
	 */
	void delete(final int id);

	/**
	 * Extracts object from the database with the given id
	 * 
	 * @param id
	 * @return
	 */
	T get(final int id);

	/**
	 * Updates object from the database with the given id
	 * 
	 * @param o
	 */
	void update(final T o);

	/**
	 * Returns a list of all the objects from the database
	 * 
	 * @return
	 */
	List<T> getAll();

}