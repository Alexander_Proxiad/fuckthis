<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<!doctype html>
<html lang="en">

<head>
<meta charset="utf-8">
<meta name="viewport"
	content="width=device-width, initial-scale=1, shrink-to-fit=no">
<link rel="stylesheet"
	href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css"
	integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm"
	crossorigin="anonymous">
<link rel="stylesheet"
	href="https://use.fontawesome.com/releases/v5.7.2/css/all.css"
	integrity="sha384-fnmOCqbTlWIlj8LyTjo7mOUStjsKC4pOpQbqyi7RrhN7udi9RwhKkMHpvLbHG9Sr"
	crossorigin="anonymous">
<title>ALEXNET</title>
<style>
.carousel-item {
	height: 500px;
}

/* CSS Test begin */
.comment-box {
	margin-top: 30px !important;
}

/* CSS Test end */
.comment-box img {
	width: 50px;
	height: 50px;
}

.comment-box .media-left {
	padding-right: 10px;
	width: 65px;
}

.comment-box .media-body p {
	border: 1px solid #ddd;
	padding: 10px;
}

.comment-box .media-body .media p {
	margin-bottom: 0;
}

.comment-box .media-heading {
	background-color: #f5f5f5;
	border: 1px solid #ddd;
	padding: 7px 10px;
	position: relative;
	margin-bottom: -1px;
}

.comment-box .media-heading:before {
	content: "";
	width: 12px;
	height: 12px;
	background-color: #f5f5f5;
	border: 1px solid #ddd;
	border-width: 1px 0 0 1px;
	-webkit-transform: rotate(-45deg);
	transform: rotate(-45deg);
	position: absolute;
	top: 10pxent left: -6px;
}

.carousel {
	overflow: hidden
}

#commentSection {
	left: 21px;
}

#commentDiv {
	left: -51px;
}

.carousel-item {
	height: 500px;
}

body {
	background-color: rgb(51, 53, 58);
}

.jumbotron {
	background-color: rgb(42, 44, 51);
}

.card-img-top {
	width: 100%;
	height: 12vw;
	object-fit: cover;
}

span {
	float: left;
}

#fireText {
	color: #FFFFFF;
	background: #333333;
	text-shadow: 0 -1px 4px #FFF, 0 -2px 10px #ff0, 0 -10px 20px #ff8000, 0
		-18px 40px #F00;
	color: #FFFFFF;
	background: #333333;
}

#peopleSortHeading {
	text-shadow: 0px 0px 0 #899CD5, 1px 1px 0 #8194CD, 2px 2px 0 #788BC4,
		3px 3px 0 #6F82BB, 4px 4px 0 #677AB3, 5px 5px 0 #5E71AA, 6px 6px 0
		#5568A1, 7px 7px 0 #4C5F98, 8px 8px 0 #445790, 9px 9px 0 #3B4E87, 10px
		10px 0 #32457E, 11px 11px 0 #2A3D76, 12px 12px 0 #21346D, 13px 13px 0
		#182B64, 14px 14px 0 #0F225B, 15px 15px 0 #071A53, 16px 16px 0 #02114A,
		17px 17px 0 #0B0841, 18px 18px 0 #130039, 19px 19px 0 #1C0930, 20px
		20px 0 #251227, 21px 21px 20px rgba(0, 0, 0, 1), 21px 21px 1px
		rgba(0, 0, 0, 0.5), 0px 0px 20px rgba(0, 0, 0, .2), -40px -40px 0px
		rgba(28, 110, 164, 0);
	color: #FFFFFF;
}
</style>
</head>

<body>
	<div class="container-fluid">
		<div class="row">
			<div class="col-md-12">
				<nav
					class="navbar fixed-top navbar-expand-lg navbar-light bg-light rounded">

					<button class="navbar-toggler" type="button" data-toggle="collapse"
						data-target="#bs-example-navbar-collapse-1">
						<span class="navbar-toggler-icon"></span>
					</button>
					<a class="navbar-brand" href="#">ALEXO</a>
					<div class="collapse navbar-collapse"
						id="bs-example-navbar-collapse-1">
						<ul class="navbar-nav">
							<li class="nav-item"><a class="nav-link" href="main-page">Events
									<span class="sr-only">(current)</span>
							</a></li>
							<li class="nav-item"><a class="nav-link" href="#">Schedule</a></li>
							<li class="nav-item active"><a class="nav-link"
								href="people">People</a></li>
						</ul>
						<ul class="navbar-nav ml-md-auto">
							<li class="nav-item active"><a class="nav-link" href="#">
									${sessionScope.currentUser.firstName} <span class="sr-only">(current)</span>
							</a></li>
							<li><a id="modal-383649" href="#modal-container-383649"
								role="button" class="btn" data-toggle="modal">Logout</a>

								<div class="modal fade" id="modal-container-383649"
									role="dialog" aria-labelledby="myModalLabel" aria-hidden="true"
									data-backdrop="false">
									<div class="modal-dialog" role="document">
										<div class="modal-content">
											<div class="modal-header">
												<h5 class="modal-title" id="myModalLabel">Do you really
													want to log out?</h5>
												<button type="button" class="close" data-dismiss="modal">
													<span aria-hidden="true">�</span>
												</button>
											</div>
											<div class="modal-footer">
												<form:form action="/logout" method="get">
													<input type="submit" value="Yes" class="btn btn-danger" />
												</form:form>
												<button type="button" class="btn btn-secondary"
													data-dismiss="modal">Close</button>
											</div>
										</div>

									</div>

								</div>
					</div>
			</div>
			</li>
			</ul>
		</div>
		</nav>
		<div class="container-fluid">
			<center>
				<div class="h1 mt-5" id="peopleSortHeading">
					Get to know your coleagues! In this section you can see all the
					registered users and you can filter them as you like!<small>So
						let's go!</small>
				</div>
			</center>
			<div class="row mt-5">
				<div class="col-md-12">
					<div class="input-group mb-3 col-md-6 offset-md-3">
						<input type="text" class="form-control" placeholder="Find by name"
							aria-label="Recipient's username" aria-describedby="basic-addon2"
							id="search">
						<div class="input-group-append">
							<div class="input-group-append">
								<button class="btn btn-dark sortByName" type="button"
									id="fireText">Name</button>
							</div>
							<div class="input-group-append">
								<button class="btn btn-dark ageButton" type="button" id="fireText">Age</button>
							</div>
							<div class="input-group-append">
								<button class="btn btn-dark joinedOnButton" type="button" id="fireText">Joined
									On</button>
							</div>
						</div>
					</div>
					<center>
						<div class="row mt-5 ml-3">
							<div class="col-md-12">
								<center>
									<div class="row employeeList">
										<c:forEach items="${AllEmployees}" var="currentEmployee">
											<div class="card mr-5 ml-4 mt-3 sort-item sortByAge" style="width: 18rem;"
											data-event-date="${currentEmployee.joinedOn}" data-name="${currentEmployee.age}">
												<img class="card-img-top"
													src="getEmployeePhoto/${currentEmployee.id}"
													alt="Card image cap">
												<div class="card-body">
													<h4 class="card-title">${currentEmployee.firstName}
														${currentEmployee.lastName}</h4>
												</div>
												<p class="card-text">
													<span class="badge badge-dark rounded-circle"
														style="float: center"><i class="fas fa-briefcase"></i></span>${currentEmployee.authority}
												</p>
												<p class="card-text">
													<span class="badge badge-dark rounded-circle"
														style="float: center"><i
														class="fas fa-calendar-alt"></i></span>${currentEmployee.age}
												</p>
												<p class="card-text">
													<span class="badge badge-dark rounded-circle"
														style="float: center"><i class="fas fa-envelope"></i></span>${currentEmployee.email}
												</p>
												<div class="card-footer rounded-circle">
													<small class="text-muted">Joined on:
														${currentEmployee.joinedOn}</small>
												</div>
											</div>
										</c:forEach>
									</div>
								</center>
							</div>
						</div>
				</div>
				<script src="https://code.jquery.com/jquery-3.2.1.slim.min.js"
					integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN"
					crossorigin="anonymous"></script>
				<script
					src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js"
					integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q"
					crossorigin="anonymous"></script>
				<script
					src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js"
					integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl"
					crossorigin="anonymous"></script>
				<script>
				 $(document).ready(
	                        function () {
	                            $('#search').keyup(
	                                function () {
	                                    $('.card').removeClass('d-none');
	                                    var filter = $(this).val();
	                                    $('.employeeList').find(
	                                        '.card .card-body h4:not(:contains("'
	                                        + filter + '"))')
	                                        .parent().parent()
	                                        .addClass('d-none');
	                                })
	                            $(".sortByName").click(function () {
	                                $(function () {

	                                    var $list = $(".employeeList");

	                                    $list.children().detach().sort(function (a, b) {
	                                        return $(a).text().localeCompare($(b).text());
	                                    }).appendTo($list);

	                                });
	                            });
	                            $(".joinedOnButton").click(function () {
	                                (function ($) {
	                                    var container = $(".employeeList");
	                                    var items = $(".sort-item");

	                                    items.each(function () {
	                                        var BCDate = $(this).attr("data-event-date").split("-");
	                                        var standardDate = BCDate[1] + " " + BCDate[0] + " " + BCDate[2];
	                                        standardDate = new Date(standardDate).getTime();
	                                        $(this).attr("data-event-date", standardDate);

	                                    });


	                                    items.sort(function (a, b) {
	                                        a = parseFloat($(a).attr("data-event-date"));
	                                        b = parseFloat($(b).attr("data-event-date"));
	                                        return a > b ? -1 : a < b ? 1 : 0;
	                                    }).each(function () {
	                                        container.prepend(this);
	                                    });

	                                })(jQuery);
	                            });
	                            $(".ageButton").click(function () {
	                                var $wrapper = $('.employeeList');

	                                $wrapper.find('.sortByAge').sort(function (a, b) {
	                                    return +a.dataset.name - +b.dataset.name;
	                                })
	                                    .appendTo($wrapper);
	                                });
	                        });
				</script>
</body>

</html>