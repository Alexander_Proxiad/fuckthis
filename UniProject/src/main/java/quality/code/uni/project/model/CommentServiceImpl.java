package quality.code.uni.project.model;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class CommentServiceImpl implements CommentService {

	@Autowired
	private CommentDao commentDao;

	@Override
	public void saveComment(Comment comment) {
		commentDao.save(comment);
	}

	@Override
	public List<Comment> getCommentsForIdeaWithIdeaId(int id) {
		return commentDao.getCommentsForIdeaWithIdeaId(id);
	}

}
