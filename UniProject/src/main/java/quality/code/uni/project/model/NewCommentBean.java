package quality.code.uni.project.model;

/**
 * Entity like class that stores the parameters from the form that is used to
 * create a new comment to a specific theme
 * 
 * @author a.vulchev
 *
 */
public class NewCommentBean {

	private String comment;
	private int topicId;
	private int userId;

	/**
	 * No-args constructor for class "NewCommentBean"
	 */
	public NewCommentBean() {
		super();
	}

	/**
	 * All args constructor for class "NewCommentBean"
	 * 
	 * @param comment
	 * @param topicId
	 * @param userId
	 */
	public NewCommentBean(String comment, int topicId, int userId) {
		super();
		this.comment = comment;
		this.topicId = topicId;
		this.userId = userId;
	}

	public String getComment() {
		return comment;
	}

	public void setComment(String comment) {
		this.comment = comment;
	}

	public int getTopicId() {
		return topicId;
	}

	public void setTopicId(int topicId) {
		this.topicId = topicId;
	}

	public int getUserId() {
		return userId;
	}

	public void setUserId(int userId) {
		this.userId = userId;
	}

}
